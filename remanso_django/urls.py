"""remanso_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from Apps.personal.views import PersonalAllViewSet, PersonalViewSet, ClientesViewSet, ClientesAllViewSet
from Apps.proveedor.urls import *
from remansoApp.views import *

router = routers.DefaultRouter()
rest_base_url = "api/"

router.register(rest_base_url + "usuarios", UsuarioViewSet)
router.register(rest_base_url + "perfiles", PerfilesViewSet)
router.register(rest_base_url + "proveedores", ProveedoresViewSet)
router.register(rest_base_url + "proveedor", ProveedorViewSet)
router.register(rest_base_url + "personalAll", PersonalAllViewSet)
router.register(rest_base_url + "productos", ProductoViewSet)
router.register(rest_base_url + "productos_proveedor", Categoria_Producto_Serializer)

router.register(rest_base_url + "personal", PersonalViewSet)
router.register(rest_base_url + "clientes", ClientesViewSet)
router.register(rest_base_url + "clientesAll", ClientesAllViewSet)
router.register(rest_base_url + "platos", PlatosViewSet)
router.register(rest_base_url + "ventas", VentasViewSet)
router.register(rest_base_url + "pedidos", PedidosViewSet)




urlpatterns = [
    url(r'^$',login_required(HomeTemplateView.as_view()), name='home'),
    url(r'^login/$', auth_views.login, {'template_name': 'home/login.html'}, name="login"),
    url(r'^logout/$', auth_views.logout, {'next_page': 'home'}, name='logout'),

    url(r'^', include(router.urls)),
    url(r'^proveedor/', include('Apps.proveedor.urls')),
    url(r'^menu/', include('Apps.menu.urls')),
    url(r'^pedidos/', include('Apps.pedido.urls')),
    url(r'^personal/', include('Apps.personal.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api-token-auth/', obtain_jwt_token)

]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
