from django.apps import AppConfig


class RemansoappConfig(AppConfig):
    name = 'remansoApp'
