import os
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from rest_framework.viewsets import ModelViewSet

from Apps.pedido.models import Mesa
from Apps.proveedor.models import Proveedor
from remansoApp.serializers import *
from datetime import datetime
from django.views.generic import TemplateView, CreateView, ListView, DetailView, UpdateView
from django.db.models import Sum

import re

estados = (('1', 'Activo'), ('0', 'Inactivo'),)
generos = (('Masculino', 'Masculino'), ('Femenino', 'Femenino'), ('Otro', 'Otro'),)
pathImgPlatos = os.path.join(settings.MEDIA_ROOT, "imgPlatos")


# ----------------------Serializaer API--------------------------
class UsuarioViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UsuariosSerializer


class PerfilesViewSet(ModelViewSet):
    queryset = Perfil.objects.all()
    serializer_class = PerfilesSerializer


class PedidosViewSet(ModelViewSet):
    queryset = Pedido.objects.all()
    serializer_class = PedidosSerializer


class PlatosViewSet(ModelViewSet):
    queryset = Menu.objects.all()
    serializer_class = PlatosSerializer


class VentasViewSet(ModelViewSet):
    queryset = Factura.objects.all()
    serializer_class = VentasSerializer


# ---------------------Views---------------------------
class HomeTemplateView(TemplateView):
    template_name = "home/home.html"

    def get_context_data(self, *args, **kwargs):
        context = super(HomeTemplateView, self).get_context_data(*args, **kwargs)
        context['titulo'] = "Inicio"
        context['inicio_activo'] = 'active'
        # Estadisticas Diarias
        context['total_clientes'] = Cliente.objects.all().count()
        context['total_pedidos'] = Pedido.objects.filter(fecha__lte=datetime.today()).count()
        context['total_ventas'] = Factura.objects.filter(fecha__lte=datetime.today()).aggregate(Sum('total'))
        context['total_mesas'] = Mesa.objects.filter(diponible=True).count()
        context['mesas'] = Mesa.objects.all()


        # Prueba
        text = "A56B455VB23GTY23J"

        text = "A56B455VB23GTY23J"
        text = re.findall('\d+', text)
        numeros= list(map(int, text))
        print(sorted(set(numeros)))

        return context




def getNumber():
    text = "A56B455VB23GTY23J"







