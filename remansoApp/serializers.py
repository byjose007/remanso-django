from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
# Apps models
from Apps.menu.models import Menu
from Apps.pedido.models import Pedido
from Apps.personal.models import Personal, Cliente, Perfil
from Apps.proveedor.models import Proveedor
from Apps.venta.models import Factura


class UsuariosSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class PerfilesSerializer(ModelSerializer):
    class Meta:
        model = Perfil
        fields = '__all__'


class PedidosSerializer(ModelSerializer):
    class Meta:
        model = Pedido
        fields = '__all__'


class PlatosSerializer(ModelSerializer):
    class Meta:
        model = Menu
        fields = '__all__'


class VentasSerializer(ModelSerializer):
    class Meta:
        model = Factura
        fields = '__all__'
