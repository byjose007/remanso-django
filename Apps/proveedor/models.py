from django.db import models
from Apps.personal.models import Perfil


# Create your models here.
# ---------------------------------------------
class Proveedor(models.Model):
    perfil = models.ForeignKey(Perfil, related_name='proveedores', on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % (self.perfil.nombres)

    class Meta:
        verbose_name_plural = 'Proveedores'


# ---------------------------------------------
class CategoriaProducto(models.Model):
    nombre = models.CharField(verbose_name='Categoría', blank=True, null=True, max_length=50)

    def __str__(self):
        return '%s' % (self.nombre)

    class Meta:
        verbose_name_plural = 'Categorias'


# ---------------------------------------------
UNIDAD_MEDIDA = (('Kilo', 'Kilo'), ('Libra', 'Libra'), ('Onza', 'Onza'), ('Porcion', 'Porción'), ('Unidad', 'Unidad'))


class Producto(models.Model):
    nombre = models.CharField(verbose_name='Nombre', max_length=50)
    categoria = models.ForeignKey(CategoriaProducto, verbose_name='Categoría', blank=True, null=True, on_delete=models.CASCADE)
    proveedor = models.ForeignKey(Proveedor, verbose_name='Proveedor',  blank=True, null=True, on_delete=models.CASCADE)
    fecha_caducidad = models.DateField(verbose_name='Fecha de caducidad', blank=True, null=True)
    imagen = models.ImageField(null=False, blank=True, upload_to='productos', verbose_name='Ruta imagen')
    unidad_medida = models.CharField(verbose_name='Unidad de medida', null=True, blank=True, max_length=10, choices=UNIDAD_MEDIDA)
    cantidad = models.IntegerField(verbose_name='Cantidad', null=True, blank=True)
    existencia_actual = models.IntegerField(verbose_name='Existencia Actual', null=True, blank=True)
    precio = models.FloatField(verbose_name='Precio Unitario', null=True, blank=True)
    alerta = models.IntegerField(verbose_name='Alerta, cuando catidad es menor a', null=True, blank=True)


    def __str__(self):
        return '%s' % (self.nombre)

