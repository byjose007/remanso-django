from django.apps import AppConfig


class ProveedorConfig(AppConfig):
    name = 'Apps.proveedor'
