from django.forms import ModelForm
from django import forms

from Apps.personal.models import Perfil
from Apps.proveedor.models import *


class ProveedorForm(ModelForm):
    cedula = forms.IntegerField(label='Cédula/Ruc')
    class Meta:
        model = Perfil
        fields = '__all__'
        exclude = ['id','genero','usuario','foto','fecha_nacimiento']



class ProductoForm(ModelForm):
    fecha_caducidad = forms.DateField(label='Fecha de caducidad', required=False, widget=forms.TextInput(attrs={'type': 'date'}))
    class Meta:
        model = Producto
        fields = '__all__'
        exclude = ['id','existencia_actual']


class ProductoUpdateForm(ModelForm):
    fecha_caducidad = forms.DateField(label='Fecha de caducidad', required=False, widget=forms.TextInput(attrs={'type': 'date'}))
    class Meta:
        model = Producto
        fields = '__all__'
        exclude = ['id','existencia_actual', 'cantidad']



class CategoriaForm(ModelForm):
    class Meta:
        model = CategoriaProducto
        fields = '__all__'
        exclude = ['id']




