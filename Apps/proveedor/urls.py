"""remanso_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required

from Apps.proveedor.views import *

app_name = 'proveedor'
urlpatterns = [
    # proveedores
    url(r'^proveedor_list/$', login_required(ProveedoresListView.as_view()), name='proveedor_list'),
    url(r'^proveedor_detail/(?P<pk>\d+)$', ProveedoresDetailView.as_view(), name='proveedor_detail'),
    url(r'^proveedor_add/$', proveedor_create, name='proveedor_add'),
    url(r'^proveedor_update/(?P<pk>\d+)$', Update.as_view(), name='proveedor_update'),
    url(r'^proveedor_delete/(?P<pk>\d+)$', Delete.as_view(), name='proveedor_delete'),

    # productos
    url(r'^producto_list/$', login_required(ProductosListView.as_view()), name='producto_list'),
    url(r'^producto_detail/(?P<pk>\d+)$', ProductosDetailView.as_view(), name='producto_detail'),
    url(r'^producto_add/$', productoCreate, name='producto_add'),
    url(r'^producto_update/(?P<pk>\d+)$', ProductoUpdate.as_view(), name='producto_update'),
    url(r'^producto_delete/(?P<pk>\d+)$', ProductoDelete.as_view(), name='producto_delete'),

    # Categorias Productos
    url(r'^categoria_create/$', categoriaCreate, name='categoria_create'),

    # Actualizar Stock
    url(r'^actualizar_stock/(?P<pk>\d+)$', actualizarStock, name='actualizar_stock')

]
