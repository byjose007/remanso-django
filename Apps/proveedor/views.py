from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, ListView, UpdateView, DetailView, DeleteView
from rest_framework import generics
from rest_framework.viewsets import ModelViewSet
from django.contrib.admin.views.decorators import staff_member_required

from Apps.personal.models import Perfil
from Apps.proveedor.forms import ProveedorForm, ProductoForm, CategoriaForm, ProductoUpdateForm
from Apps.proveedor.models import Proveedor, Producto
from Apps.proveedor.serializers import *


# -------- API REST ----------------
class ProveedoresViewSet(ModelViewSet):
    queryset = Proveedor.objects.all()
    serializer_class = ProveedoresSerializer


class ProveedorViewSet(ModelViewSet):
    queryset = Proveedor.objects.all()
    serializer_class = ProveedorSerializer


class ListaProveedores(generics.ListAPIView):
    serializer_class = ProveedoresSerializer

    def get_queryset(self):
        id_proveedor = self.kwargs['id_proveedor']
        proveedores = Proveedor.objects.filter(pk=id_proveedor)
        return proveedores


class ProductoViewSet(ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer


class Categoria_Producto_Serializer(ModelViewSet):
    queryset = CategoriaProducto.objects.all()
    serializer_class = CategoriaProductoSerializer


# class ProveedorProductosViewSet(ModelViewSet):
#     queryset = Proveedor_Producto.objects.all()
#     serializer_class = ProvededorProductosSerializer

# class ProveedorProductosAllViewSet(ModelViewSet):
#     queryset = Proveedor_Producto.objects.all()
#     serializer_class = ProvededorProductosAllSerializer


# -------- VIEW PROVEEDOR-----------

class ProveedoresDetailView(DetailView):
    model = Proveedor
    template_name = 'proveedor/proveedor_list.html'


# --------------------------------------

class ProveedoresListView(ListView):
    model = Proveedor
    template_name = 'proveedor/proveedor_list.html'

    def get_queryset(self, *args, **kwargs):
        qs = super(ProveedoresListView, self).get_queryset(*args, **kwargs)
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(ProveedoresListView, self).get_context_data(*args, **kwargs)
        context['titulo'] = "Proveedores"
        context['proveedor_activo'] = 'active'
        return context


# --------------------------------------

def proveedor_create(request):
    form_proveedor = ProveedorForm(request.POST or None)
    if request.method == 'POST':
        if all([form_proveedor.is_valid()]):
            perfil = form_proveedor.save()
            Proveedor.objects.create(perfil=perfil)
            return redirect('proveedor:proveedor_list')

    return render(request, 'proveedor/proveedor_add.html', {'form_proveedor': form_proveedor})


# --------------------------------------

class StaffRequiredMixin(object):
    @method_decorator(staff_member_required)
    def dispatch(self, request, *args, **kwargs):
        return super(StaffRequiredMixin, self).dispatch(request, *args, **kwargs)


# --------------------------------------
class Update(UpdateView):
    model = Perfil
    form_class = ProveedorForm
    template_name = "proveedor/proveedor_update.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Update, self).get_context_data(*args, **kwargs)
        context['titulo'] = "Proveedores"
        return context

    def get_success_url(self):
        return reverse('proveedor:proveedor_list')


# --------------------------------------
class Delete(DeleteView):
    model = Perfil
    template_name = "proveedor/proveedor_delete.html"

    def get_success_url(self):
        return reverse('proveedor:proveedor_list')


# -------- VIEWS PRODUCTOS-----------

class ProductosDetailView(DetailView):
    model = Producto
    template_name = 'productos/producto_list.html'


# --------------------------------------
class ProductosListView(ListView):
    model = Producto
    template_name = 'productos/producto_list.html'

    def get_queryset(self, *args, **kwargs):
        qs = super(ProductosListView, self).get_queryset(*args, **kwargs)
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(ProductosListView, self).get_context_data(*args, **kwargs)
        context['titulo'] = "Productos"
        context['producto_activo'] = 'active'
        return context


# --------------------------------------
def productoCreate(request):
    form_producto = ProductoForm(request.POST or None)
    contex = {'form_producto': form_producto, 'titulo': 'Compras', 'titulo_modal': 'Nuevo Producto'}
    if request.method == 'POST':
        if form_producto.is_valid():
            producto = form_producto.save(commit=False)
            producto.existencia_actual = request.POST.get('cantidad')
            producto.save()
            return redirect('proveedor:producto_list')
    return render(request, 'productos/producto_add.html', contex)


# --------------------------------------
class StaffRequiredMixin(object):
    @method_decorator(staff_member_required)
    def dispatch(self, request, *args, **kwargs):
        return super(StaffRequiredMixin, self).dispatch(request, *args, **kwargs)


# --------------------------------------

class ProductoUpdate(UpdateView):
    model = Producto
    form_class = ProductoUpdateForm
    template_name = "productos/producto_update.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductoUpdate, self).get_context_data(*args, **kwargs)
        context['titulo'] = "Producto"
        context['titulo_modal'] = "Actualizar Producto"
        return context

    def get_success_url(self):
        return reverse('proveedor:producto_list')


# --------------------------------------
class ProductoDelete(DeleteView):
    model = Producto

    template_name = "productos/producto_delete.html"

    def get_success_url(self):
        return reverse('proveedor:producto_list')


# --------------------------------------
def categoriaCreate(request):
    form_categoria = CategoriaForm(request.POST or None)
    contex = {'form_categoria': form_categoria, 'titulo': 'Compras', 'titulo_modal': 'Producto'}
    if request.method == 'POST':
        if form_categoria.is_valid():
            form_categoria.save()
            return redirect('proveedor:producto_add')
    return render(request, 'productos/producto_categoria.html', contex)


# --------------------------------------

def actualizarStock(request, pk):
    try:
        producto = Producto.objects.get(pk=pk)
        cantidad_anterior = producto.existencia_actual
        if request.method == 'POST':
            cantidad_actual = int(request.POST.get('cantidad'))
            if cantidad_actual != cantidad_anterior:
                cantidad = cantidad_actual
                Producto.objects.filter(pk=pk).update(existencia_actual=cantidad, cantidad=cantidad)
            return redirect('proveedor:producto_list')
    except:
        pass

    return render(request, 'productos/actualizar_stock.html', {'producto': pk, 'titulo_modal':'Actualizar Stock', 'cantidad_anterior': cantidad_anterior })

