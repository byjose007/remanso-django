from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
# Apps models

from Apps.proveedor.models import Proveedor, Producto, CategoriaProducto


class ProveedoresSerializer(ModelSerializer):
    class Meta:
        model = Proveedor
        fields = '__all__'
        depth = 2


class ProveedorSerializer(ModelSerializer):
    class Meta:
        model = Proveedor
        fields = '__all__'




class ProductoSerializer(ModelSerializer):
    class Meta:
        model = Producto
        fields = '__all__'
        depth = 1


class CategoriaProductoSerializer(ModelSerializer):
    class Meta:
        model = CategoriaProducto
        fields = '__all__'


# class ProvededorProductosAllSerializer(ModelSerializer):
#     class Meta:
#         model = Producto
#         fields = '__all__'
#         depth = 1