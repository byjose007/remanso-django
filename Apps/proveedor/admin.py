from django.contrib import admin
from Apps.proveedor.models import Proveedor, Producto, CategoriaProducto

admin.site.register(Proveedor)
admin.site.register(Producto)
admin.site.register(CategoriaProducto)
