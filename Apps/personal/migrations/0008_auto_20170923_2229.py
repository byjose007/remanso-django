# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-24 03:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('personal', '0007_perfil_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perfil',
            name='celular',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='ciudad',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='personal',
            name='tipo',
            field=models.CharField(blank=True, choices=[('Administrador', 'Administrador'), ('Cocinero', 'Cocinero'), ('Mesero', 'Mesero'), ('Otro', 'Otro')], max_length=10, null=True),
        ),
    ]
