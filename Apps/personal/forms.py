from django.forms import ModelForm
from django import forms

from Apps.personal.models import Perfil, Personal, Cliente
from Apps.proveedor.models import Proveedor


class PersonalForm(ModelForm):
    fecha_ingreso = forms.DateField(label='Fecha de ingreso',required=True, widget=forms.TextInput(attrs={'type': 'date'}))
    class Meta:
        model = Personal
        fields = '__all__'
        exclude = ['id', 'fecha_salida', 'perfil']



class PerfilForm(ModelForm):
    fecha_nacimiento = forms.DateField(label='Fecha de nacimiento', required=False, widget=forms.TextInput(attrs={'type': 'date'}))
    class Meta:
        model = Perfil
        fields = '__all__'
        exclude = ['id','usuario']


class ClienteForm(ModelForm):
    class Meta:
        model = Cliente
        fields = '__all__'
        exclude = ['id', 'perfil', 'observaciones', 'pedidos_activos' ]


