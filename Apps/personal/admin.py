from django.contrib import admin

# Register your models here.
from Apps.personal.models import Perfil, Cliente, Personal

admin.site.register(Perfil)
admin.site.register(Cliente)
admin.site.register(Personal)
