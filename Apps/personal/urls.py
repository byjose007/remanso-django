"""remanso_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required

from Apps.personal.views import *

app_name = 'personal'


urlpatterns = [
    url(r'^personal_list/$', login_required(PersonalListView.as_view()), name='personal_list'),
    url(r'^personal_add/$', personal_create, name='personal_add'),
    url(r'^personal_update/(?P<pk>\d+)$', update, name='personal_update'),
    url(r'^personal_delete/(?P<pk>\d+)$', Delete.as_view(), name='personal_delete'),

    url(r'^cliente_list/$', login_required(ClienteListView.as_view()), name='cliente_list'),
    url(r'^cliente_add/$', createCliente, name='cliente_add'),
    url(r'^cliente_update/(?P<pk>\d+)$', updateCliente, name='cliente_update'),
    url(r'^cliente_delete/(?P<pk>\d+)$', DeleteCliente.as_view(), name='cliente_delete'),


]
