from django.contrib.auth.models import User
from django.db import models

# Create your models her
#
CHOICE_GENERO = (('Masculino', 'Masculino'), ('Femenino', 'Femenino'),)


class Perfil(models.Model):
    nombres = models.CharField(max_length=150, blank=True, null=True)
    cedula = models.CharField(verbose_name='Cédula/Ruc', max_length=15, blank=False, null=False)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    genero = models.CharField(verbose_name='Género', null=True, blank=True, max_length=10, choices=CHOICE_GENERO)
    celular = models.CharField(verbose_name='Teléfono/Celular', max_length=10, blank=True, null=True)
    ciudad = models.CharField(max_length=50, blank=True, null=True)
    direccion = models.CharField(verbose_name='Dirección', max_length=50, blank=True, null=True)
    foto = models.ImageField(null=True, blank=True, upload_to='usuarioFoto', verbose_name='Ruta de foto')
    usuario = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    email = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return '%s - %s' % (self.cedula, self.nombres)


# ---------------------------------------------
class Cliente(models.Model):
    # fecha_suscripcion = models.DateField()
    # fecha_pago = models.DateField(blank=True, null=True)
    observaciones = models.CharField(max_length=250, blank=True, null=True)
    pedidos_activos = models.NullBooleanField(blank=True, null=True)
    perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % (self.perfil)

    class Meta:
        verbose_name_plural = 'Clientes'


# ---------------------------------------------
TIPO_PERSONAL = (('Administrador', 'Administrador'), ('Cocinero', 'Cocinero'), ('Mesero', 'Mesero'), ('Otro', 'Otro'))


# TIPO_PERSONAL = (('mañana', 'mañana'), ('tarde', 'tarde'), ('noche', 'noche'))


class Personal(models.Model):
    fecha_ingreso = models.DateField()
    fecha_salida = models.DateField(blank=True, null=True)
    tipo = models.CharField(null=True, blank=True, max_length=20, choices=TIPO_PERSONAL)
    perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)

    # cod_turno = models.CharField(null=True, blank=True, choices=TIPO_turno)

    def __str__(self):
        return '%s' % (self.perfil)

    class Meta:
        verbose_name_plural = 'Personal'
# ---------------------------------------------
