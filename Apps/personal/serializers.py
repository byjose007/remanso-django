from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
# Apps models
from Apps.personal.models import Personal, Cliente
from Apps.proveedor.models import Proveedor

# -------------- Personal Interno---------------------
class PersonalSerializer(ModelSerializer):
    class Meta:
        model = Personal
        fields = '__all__'

class PersonalAllSerializer(ModelSerializer):
    class Meta:
        model = Personal
        fields = '__all__'
        depth = 2


# -------------- Clientes ---------------------
class ClientesSerializer(ModelSerializer):
    class Meta:
        model = Cliente
        fields = '__all__'

class ClientesAllSerializer(ModelSerializer):
    class Meta:
        model = Cliente
        fields = '__all__'
        depth = 2
