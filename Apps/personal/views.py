from django.shortcuts import render, redirect

# Create your views here.
# from django.views.generic import DetailView
from django.urls import reverse
from rest_framework.viewsets import ModelViewSet

from Apps.personal.forms import PersonalForm, PerfilForm, ClienteForm
from Apps.personal.models import Personal, Cliente, Perfil
from Apps.personal.serializers import PersonalSerializer, PersonalAllSerializer, ClientesSerializer, ClientesAllSerializer

from django.views.generic import CreateView, ListView, UpdateView, DetailView, DeleteView


# -------------- Personal Interno---------------------

class PersonalViewSet(ModelViewSet):
    queryset = Personal.objects.all()
    serializer_class = PersonalSerializer


class PersonalAllViewSet(ModelViewSet):
    queryset = Personal.objects.all()
    serializer_class = PersonalAllSerializer


# -------------- Clientes ---------------------

class ClientesViewSet(ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClientesSerializer


class ClientesAllViewSet(ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClientesAllSerializer


# -------- VIEWS PERSONAL -----------

# class PersonalDetailView(DetailView):
#     model = Personal
#     template_name = 'personal/personal_list.html'


# --------------------------------------


class PersonalListView(ListView):
    model = Personal
    template_name = 'personal/personal_list.html'

    # def get_queryset(self, *args, **kwargs):
    #     if self.kwargs['tipo'] == 'personal':
    #         self.model = Personal.objects.all()
    #         titulo = 'Personal'
    #     elif self.kwargs['tipo'] == 'clientes':
    #         self.model = Cliente.objects.all()
    #         titulo = 'Clientes'
    #     return self.model

    def get_context_data(self, *args, **kwargs):
        context = super(PersonalListView, self).get_context_data(*args, **kwargs)
        context['titulo'] = 'Personal'
        context['personal_activo'] = 'active'
        return context


# --------------------------------------

def personal_create(request):
    form_perfil = PerfilForm(request.POST or None, request.FILES)
    form_personal = PersonalForm(request.POST or None)
    if request.method == 'POST':
        if all([form_personal.is_valid(), form_perfil.is_valid()]):
            perfil = form_perfil.save(commit=False)
            perfil.save()
            personal = form_personal.save(commit=False)
            personal.perfil_id = perfil.pk
            personal.save()
            return redirect('personal:personal_list')

    return render(request, 'personal/personal_add.html', {'form_perfil': form_perfil, 'form_personal': form_personal})


# --------------------------------------
#
# class StaffRequiredMixin(object):
#     @method_decorator(staff_member_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super(StaffRequiredMixin, self).dispatch(request, *args, **kwargs)


# --------------------------------------
def update(request, pk):
    personal = Personal.objects.get(pk=pk)
    perfil = Perfil.objects.get(pk=personal.perfil.pk)
    form_perfil = PerfilForm(request.POST or None, instance=perfil)
    form_personal = PersonalForm(request.POST or None, instance=personal)
    if request.method == 'POST':
        if all([form_personal.is_valid(), form_perfil.is_valid()]):
            form_perfil.save()
            form_personal.save()
            return redirect('personal:personal_list')
    return render(request, 'personal/personal_update.html', {'form_perfil': form_perfil, 'form_personal': form_personal, 'titulo': 'Personal'})


# # --------------------------------------
class Delete(DeleteView):
    model = Personal
    template_name = "personal/personal_delete.html"

    def get_success_url(self):
        return reverse('personal:personal_list')

# --------------------------------------



# -------- VIEWS CLIENTES -----------

# class PersonalDetailView(DetailView):
#     model = Personal
#     template_name = 'personal/personal_list.html'


# --------------------------------------


class ClienteListView(ListView):
    model = Cliente
    template_name = 'cliente/cliente_list.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ClienteListView, self).get_context_data(*args, **kwargs)
        context['titulo'] = 'Cliente'
        context['cliente_activo'] = 'active'
        return context


# --------------------------------------

def createCliente(request):
    form_perfil = PerfilForm(request.POST or None, request.FILES)
    form_cliente = ClienteForm()
    contex = {'form_perfil': form_perfil,'titulo':'Cliente', 'titulo_modal':'Nuevo Cliente'}
    if request.method == 'POST':
        if form_perfil.is_valid():
            perfil = form_perfil.save(commit=False)
            perfil.save()
            cliente= form_cliente.save(commit=False)
            cliente.perfil_id = perfil.pk
            cliente.save()
            return redirect('personal:cliente_list')

    return render(request, 'cliente/cliente_add.html', contex)


# --------------------------------------
#
# class StaffRequiredMixin(object):
#     @method_decorator(staff_member_required)
#     def dispatch(self, request, *args, **kwargs):
#         return super(StaffRequiredMixin, self).dispatch(request, *args, **kwargs)


# --------------------------------------
def updateCliente(request, pk):
    cliente = Cliente.objects.get(pk=pk)
    perfil = Perfil.objects.get(pk=cliente.perfil.pk)
    form_perfil = PerfilForm(request.POST or None, instance=perfil)
    contex = {'form_perfil': form_perfil, 'titulo': 'Cliente', 'titulo_modal': 'Actualizar Cliente'}
    if request.method == 'POST':
        if form_perfil.is_valid():
            form_perfil.save()
            # form_personal.save()
            return redirect('personal:cliente_list')
    return render(request, 'cliente/cliente_update.html', contex)


# # --------------------------------------
class DeleteCliente(DeleteView):
    model = Cliente
    template_name = "cliente/cliente_delete.html"

    def get_success_url(self):
        return reverse('personal:cliente_list')

# --------------------------------------