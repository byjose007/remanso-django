from django.apps import AppConfig


class MenuConfig(AppConfig):
    name = 'Apps.menu'
