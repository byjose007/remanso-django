from django.db import models
from Apps.proveedor.models import Producto


# Create your models here.

# ---------------------------------------------------------------------------

class Menu_Categoria(models.Model):
    categoria = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % (self.categoria)


# ---------------------------------------------


TIPO_MENU = (('carta', 'A la Carta'), ('desayuno', 'Desayuno'), ('almuerzo', 'Almuerzo'), ('merienda', 'Merienda'),
             ('bebida', 'Bebida'), ('otro', 'Otro'))


class Menu(models.Model):
    tipo = models.CharField(null=True, blank=True, max_length=10, choices=TIPO_MENU)
    nombre = models.CharField(verbose_name='Nombre',max_length=200)
    imagen = models.ImageField(null=False, blank=True, upload_to='menuFoto', verbose_name='Imágen')
    descripcion = models.CharField(verbose_name='Descripcción',max_length=200, null=True, blank=True)
    precio = models.FloatField(verbose_name='Precio',null=False, blank=False)
    categoria = models.ForeignKey(Menu_Categoria, verbose_name='Categoría',on_delete=models.CASCADE, null=True, blank=True)
    cantidad_diaria = models.IntegerField(verbose_name='Cantidad',null=True, blank=True)
    existencia_actual = models.IntegerField(verbose_name='En Stock',null=True, blank=True)
    alerta = models.IntegerField(verbose_name='Alerta, cuando catidad es menor a', null=True, blank=True)
    activo = models.BooleanField(default=True)


    def __str__(self):
        return "%s" % (self.nombre)

    class Meta:
        verbose_name_plural = 'Menu'


# ---------------------------------------------

class Producto_Menu(models.Model):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad_prod_utilizado = models.IntegerField()
