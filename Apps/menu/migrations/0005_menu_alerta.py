# Generated by Django 2.0 on 2017-12-18 22:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0004_auto_20171218_1645'),
    ]

    operations = [
        migrations.AddField(
            model_name='menu',
            name='alerta',
            field=models.IntegerField(blank=True, null=True, verbose_name='Alerta, cuando catidad es menor a'),
        ),
    ]
