from django.forms import ModelForm
from django import forms

from Apps.menu.models import Menu, Menu_Categoria
from Apps.personal.models import Perfil
from Apps.proveedor.models import *


class MenuForm(ModelForm):
    class Meta:
        model = Menu
        fields = '__all__'
        exclude = ['id','tipo', 'existencia_actual', 'activo']

class MenuUpdateForm(ModelForm):
    class Meta:
        model = Menu
        fields = '__all__'
        exclude = ['id','tipo', 'existencia_actual', 'cantidad_diaria']

class CategoriaMenuForm(ModelForm):
    class Meta:
        model = Menu_Categoria
        fields = '__all__'
        exclude = ['id']




