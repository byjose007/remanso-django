from django.contrib import admin

# Register your models here.
from Apps.menu.models import Menu, Producto_Menu, Menu_Categoria

admin.site.register(Menu)
admin.site.register(Producto_Menu)
admin.site.register(Menu_Categoria)
