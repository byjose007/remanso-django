"""remanso_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required

from Apps.menu.views import *

app_name = 'menu'


urlpatterns = [
    url(r'^menu_list/$', login_required(MenuListView.as_view()), name='menu_list'),
    url(r'^menu_add/$', menuCreate, name='menu_add'),
    url(r'^menu_update/(?P<pk>\d+)$', MenuUpdate.as_view(), name='menu_update'),
    url(r'^menu_delete/(?P<pk>\d+)$', MenuDelete.as_view(), name='menu_delete'),
    # Categorias Productos
    url(r'^categoria_create/$', categoriaCreate, name='categoria_create'),
    # Actualizar Stock
    url(r'^actualizar_stock/(?P<pk>\d+)$', actualizarStock, name='actualizar_stock'),

]
