from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render, redirect

# -------- VIEWS PRODUCTOS-----------
from django.urls import reverse
from django.views.generic import DetailView, ListView, UpdateView, DeleteView

from Apps.menu.forms import MenuForm, CategoriaMenuForm, MenuUpdateForm
from Apps.menu.models import Menu
from Apps.proveedor.forms import CategoriaForm


class MenuDetailView(DetailView):
    model = Menu
    template_name = 'menu/menu_list.html'


# --------------------------------------
class MenuListView(ListView):
    model = Menu
    template_name = 'menu/menu_list.html'

    def get_queryset(self, *args, **kwargs):
        qs = super(MenuListView, self).get_queryset(*args, **kwargs)
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(MenuListView, self).get_context_data(*args, **kwargs)
        context['titulo'] = "Menú"
        context['menu_activo'] = 'active'
        return context


# --------------------------------------
def menuCreate(request):
    form_menu = MenuForm(request.POST or None, request.FILES or None)
    contex = {'form_producto': form_menu, 'titulo': 'Menú', 'titulo_modal': 'Nuevo Menú'}
    if request.method == 'POST':
        if form_menu.is_valid():
            menu = form_menu.save(commit=False)
            menu.existencia_actual = request.POST.get('cantidad_diaria')
            menu.save()
            return redirect('menu:menu_list')
    return render(request, 'menu/menu_add.html', contex)


# --------------------------------------
class MenuUpdate(UpdateView):
    model = Menu
    form_class = MenuUpdateForm
    template_name = "menu/menu_update.html"

    def get_context_data(self, *args, **kwargs):
        context = super(MenuUpdate, self).get_context_data(*args, **kwargs)
        context['titulo'] = "Menú"
        context['titulo_modal'] = "Actualizar Menú"
        return context

    def get_success_url(self):
        return reverse('menu:menu_list')


# --------------------------------------
class MenuDelete(DeleteView):
    model = Menu
    template_name = "menu/menu_delete.html"

    def get_success_url(self):
        return reverse('menu:menu_list')


# --------------------------------------
def categoriaCreate(request):
    form_categoria = CategoriaMenuForm(request.POST or None)
    contex = {'form_categoria': form_categoria, 'titulo': 'Menu', 'titulo_modal': 'Categoría Menú'}
    if request.method == 'POST':
        if form_categoria.is_valid():
            form_categoria.save()
            return redirect('menu:menu_add')
    return render(request, 'menu/menu_categoria.html', contex)


# --------------------------------------

def actualizarStock(request, pk):
    try:
        menu = Menu.objects.get(pk=pk)
        cantidad_anterior = menu.existencia_actual
        if request.method == 'POST':
            cantidad_actual = int(request.POST.get('cantidad'))
            if cantidad_actual != cantidad_anterior:
                cantidad = cantidad_actual
                Menu.objects.filter(pk=pk).update(existencia_actual=cantidad, cantidad_diaria=cantidad)
            return redirect('menu:menu_list')
    except:
        pass

    return render(request, 'menu/actualizar_stock.html', {'menu': pk, 'titulo_modal':'Actualizar Stock', 'cantidad_anterior': cantidad_anterior })


