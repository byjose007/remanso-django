from django.apps import AppConfig


class PedidoConfig(AppConfig):
    name = 'Apps.pedido'
