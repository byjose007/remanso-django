from django.db import models

# Create your models here.
# # ---------------------------------------------
from Apps.menu.models import Menu
from Apps.personal.models import Cliente, Personal




# ---------------------------------------------
class EstadoPedido(models.Model):
    estado = models.CharField(max_length=50)
    # Abiertpo , cerrado

    def __str__(self):
        return '%s' % self.estado

    class Meta:
        verbose_name_plural = "Estado Pedido"

# ---------------------------------------------


class Mesa(models.Model):
    color = models.CharField(max_length=20,null=True, blank=True)
    numero = models.IntegerField(null=True, blank=True)
    diponible = models.BooleanField(default=True)

    def __str__(self):
        return '%s' % (self.numero)

    class Meta:
        verbose_name_plural = "Mesas"
        managed = True
        ordering = ['numero']


# ---------------------------------------------


class Pedido(models.Model):
    fecha = models.DateTimeField(blank=True,null=True)
    estado = models.ForeignKey(EstadoPedido, null=True, blank=True, on_delete=models.CASCADE)
    observaciones = models.TextField(max_length=250,null=True,blank=True)
    valor_total = models.FloatField(blank=True,null=True)
    tiempo = models.IntegerField(blank=True,null=True)
    notificado = models.BooleanField(default=False)
    pedido_a_nombre = models.CharField(verbose_name='Nombre Cliente',max_length=250,null=True,blank=True)
    cliente = models.ForeignKey(Cliente,null=True,blank=True,on_delete=models.CASCADE)
    camarero = models.ForeignKey(Personal,verbose_name='Mesero',null=True,blank=True,on_delete=models.CASCADE)
    mesa = models.ForeignKey(Mesa,null=True,blank=True,on_delete=models.CASCADE)


    def __str__(self):
        return '%s - %s - %s - %s' % (self.fecha, self.estado, self.valor_total, self.cliente)

    class Meta:
        verbose_name_plural = "Pedidos"
        managed = True

# ---------------------------------------------
class PedidoMenu(models.Model):
    valor = models.FloatField(null=True, blank=True)
    cantidad_solicitada = models.IntegerField(null=True, blank=True)
    pedido = models.ForeignKey(Pedido, null=False, blank=False, on_delete=models.CASCADE)
    menu = models.ForeignKey(Menu, null=False, blank=False, on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % (self.pedido)

    class Meta:
        verbose_name = 'Detalle de pedido'
        verbose_name_plural = 'Detalles de pedido'


