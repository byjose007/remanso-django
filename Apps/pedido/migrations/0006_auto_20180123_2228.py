# Generated by Django 2.0 on 2018-01-24 03:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pedido', '0005_pedido_pedido_a_nombre'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pedido',
            name='observaciones',
            field=models.TextField(blank=True, max_length=250, null=True),
        ),
    ]
