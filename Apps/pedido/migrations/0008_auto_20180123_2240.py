# Generated by Django 2.0 on 2018-01-24 03:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pedido', '0007_auto_20180123_2238'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pedido',
            name='valor_total',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
