from django.forms import ModelForm
from django import forms

from Apps.menu.models import Menu, Menu_Categoria
from Apps.pedido.models import Pedido, Mesa


class PedidoForm(ModelForm):

    class Meta:
        model = Pedido
        fields = '__all__'
        exclude = ['id', 'fecha', 'estado', 'notificado', 'tiempo']


class MenuUpdateForm(ModelForm):
    class Meta:
        model = Menu
        fields = '__all__'
        exclude = ['id','tipo', 'existencia_actual', 'cantidad_diaria']


class MesaForm(ModelForm):
    class Meta:
        model = Mesa
        fields = '__all__'
        exclude = ['id', 'color']
        order_by = ['numero']


class MesaOpenForm(ModelForm):
    class Meta:
        model = Pedido
        fields = ['pedido_a_nombre','camarero','observaciones']







