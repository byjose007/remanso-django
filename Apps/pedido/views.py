import json

from django.core import serializers

from django.http import JsonResponse
from django.shortcuts import render

from django.shortcuts import render, redirect

# -------- VIEWS PRODUCTOS-----------
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, ListView, UpdateView, DeleteView, CreateView

from Apps.menu.forms import MenuForm, CategoriaMenuForm, MenuUpdateForm
from Apps.menu.models import Menu, Menu_Categoria
from Apps.pedido.forms import PedidoForm, MesaForm, MesaOpenForm
from Apps.pedido.models import Mesa, EstadoPedido, Pedido, PedidoMenu
from Apps.proveedor.forms import CategoriaForm
from datetime import datetime


class PedidoDetailView(DetailView):
    model = Menu
    template_name = 'menu/menu_list.html'


# --------------------------------------
class PedidosListView(ListView):
    model = Pedido
    template_name = 'pedido/pedido_list.html'

    def get_queryset(self, *args, **kwargs):
        qs = super(PedidosListView, self).get_queryset(*args, **kwargs)
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(PedidosListView, self).get_context_data(*args, **kwargs)
        context['titulo'] = "Pedidos"
        context['pedido_activo'] = 'active'
        return context


# --------------------------------------
def pedidoCreate(request):
    form = PedidoForm(request.POST or None, request.FILES or None)
    contex = {'form': form, 'titulo': 'Pedido', 'titulo_modal': 'Nuevo Pedido'}
    if request.method == 'POST':
        if form.is_valid():
            pedido = form.save(commit=False)
            pedido.existencia_actual = request.POST.get('cantidad_diaria')
            pedido.save()
            return redirect('pedido:pedido_list')
    return render(request, 'pedido/pedido_add.html', contex)


#
# # --------------------------------------
# class PedidoUpdate(UpdateView):
#     model = Menu
#     form_class = MenuUpdateForm
#     template_name = "menu/menu_update.html"
#
#     def get_context_data(self, *args, **kwargs):
#         context = super(PedidoUpdate, self).get_context_data(*args, **kwargs)
#         context['titulo'] = "Pedido"
#         context['titulo_modal'] = "Actualizar Pedido"
#         return context
#
#     def get_success_url(self):
#         return reverse('menu:menu_list')
#
#
# # --------------------------------------
# class PedidoDelete(DeleteView):
#     model = Menu
#     template_name = "menu/menu_delete.html"
#
#     def get_success_url(self):
#         return reverse('menu:menu_list')
#
#
# # --------------------------------------
class MesasListView(ListView):
    model = Mesa
    template_name = 'mesas/mesa_list.html'


    def get_queryset(self, *args, **kwargs):
        qs = super(MesasListView, self).get_queryset(*args, **kwargs)
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(MesasListView, self).get_context_data(*args, **kwargs)
        context['titulo'] = "Mesas"
        context['mesa_activo'] = 'active'
        return context


# # --------------------------------------
def mesaCreate(request):
    form = MesaForm(request.POST or None)
    mesa = form.save(commit=False)
    mesa.numero = Mesa.objects.all().count() + 1
    mesa.save()
    return redirect('pedido:mesas_list')

# --------------------------------------
class MesaDelete(DeleteView):
    model = Mesa
    template_name = "mesas/mesa_delete.html"

    def get_success_url(self):
        return reverse('pedido:mesas_list')

#--------------------------------------
def mesaOpen(request,mesa_pk):
    form = MesaOpenForm(request.POST or None)
    mesa = Mesa.objects.get(pk=mesa_pk)

    if mesa.diponible == False:
        return redirect('pedido:crear_pedido', mesa=mesa.pk)
    else:
        contex = {'form': form, 'titulo': 'Mesa', 'titulo_modal': 'Abrir Mesa ' + str(mesa.numero), 'mesa': mesa_pk}

    if request.method == 'POST':

        if form.is_valid():
            pedido = form.save(commit=False)
            # pedido.estado = EstadoPedido.objects.get(estado='E') # en espera
            pedido.mesa_id = mesa.pk
            pedido.fecha = datetime.now()
            pedido.save()
            Mesa.objects.filter(pk=mesa_pk, numero=mesa.numero).update(diponible=False)
            return redirect('pedido:crear_pedido',mesa=mesa.pk)
    return render(request, 'mesas/mesa_open.html', contex)

#--------------------------------------
def CrearPedido(request, mesa):
    try:
        pedido = Pedido.objects.get(mesa=mesa, mesa__diponible=False)
        total_pedidos = Pedido.objects.filter(fecha__lte=datetime.today())
        pedidosMenu = PedidoMenu.objects.filter(pedido=pedido)
        totalCobrar = 0
        for p in pedidosMenu:
            totalCobrar += p.menu.precio * p.cantidad_solicitada
        platos = []
        categorias = Menu_Categoria.objects.all()
        for cat in categorias:
            menus = Menu.objects.filter(categoria=cat.pk).values()
            if menus:
                platos.append({'categoria': cat.categoria, 'platos': list(menus)})
        # platos = json.dumps(platos)
        contex = {'titulo': 'Pedido', 'menus': platos, 'pedido': pedido, 'total_pedidos': total_pedidos.count(), 'pedidos': pedidosMenu, 'totalCobrar':totalCobrar}

        return render(request, 'pedido/pedido.html', contex)
    except ValueError:
        print("Oops!")




#--------------------------------------
def eliminarPedido(request, pk):
    if request.method == 'POST':
            pedido = Pedido.objects.filter(pk=pk)
            Mesa.objects.filter(pk=pedido[0].mesa.pk).update(diponible=True)
            pedido.delete()

            return redirect('home')
    return render(request, 'pedido/pedido_delete.html')




# -----------------------agragar porductos a las lista de pedidos--------------------------
def agragarMenuPedido(request, mesa, menu, pedido):

    # Pedido.objects.filter(pk=pedido, mesa=mesa).update()
    existe = PedidoMenu.objects.filter(menu_id=menu, pedido_id=pedido)
    if existe:
        cantidad  = existe[0].cantidad_solicitada + 1
        existe.update(cantidad_solicitada=cantidad)
    else:
        PedidoMenu.objects.create(menu_id=menu, pedido_id=pedido, cantidad_solicitada=1)

    return redirect('pedido:crear_pedido', mesa=mesa)


def restarMenuPedido(request, mesa, menu, pedido):
    # Pedido.objects.filter(pk=pedido, mesa=mesa).update()
    existe = PedidoMenu.objects.filter(menu_id=menu, pedido_id=pedido)
    if existe:
        cantidad  = existe[0].cantidad_solicitada - 1
        if cantidad == 0:
            existe.delete()
        else:
            existe.update(cantidad_solicitada=cantidad)




    return redirect('pedido:crear_pedido', mesa=mesa)


def eliminarMenuPedido(request, mesa, menu, pedido):
    PedidoMenu.objects.filter(menu_id=menu, pedido_id=pedido).delete()
    return redirect('pedido:crear_pedido', mesa=mesa)