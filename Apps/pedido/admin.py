from django.contrib import admin

# Register your models here.
from Apps.pedido.models import Pedido, PedidoMenu, Mesa, EstadoPedido

admin.site.register(Pedido)
admin.site.register(PedidoMenu)
admin.site.register(EstadoPedido)
admin.site.register(Mesa)