
from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required

from Apps.pedido.views import *

app_name = 'pedido'
urlpatterns = [
    # url(r'^api_menus/$', api_menus, name='api_menus'),

    url(r'^pedidos_list/$', login_required(PedidosListView.as_view()), name='pedidos_list'),
    url(r'^pedido_add/$', pedidoCreate, name='pedido_add'),
    url(r'^crear_pedido/(?P<mesa>\d+)$', CrearPedido, name='crear_pedido'),
    url(r'^pedido_delete/(?P<pk>\d+)$', eliminarPedido, name='pedido_delete'),


    url(r'^mesas_list/$', login_required(MesasListView.as_view()), name='mesas_list'),
    url(r'^mesa_add/$', mesaCreate, name='mesa_add'),
    url(r'^mesa_delete/(?P<pk>\d+)$', MesaDelete.as_view(), name='mesa_delete'),
    url(r'^mesa_open/(?P<mesa_pk>\d+)$', mesaOpen, name='mesa_open'),


    url(r'^add_menu_pedido/(?P<mesa>\d+)/(?P<menu>\d+)/(?P<pedido>\d+)$', agragarMenuPedido, name='add_menu_pedido'),
    url(r'^delete_menu_pedido/(?P<mesa>\d+)/(?P<menu>\d+)/(?P<pedido>\d+)$', eliminarMenuPedido, name='delete_menu_pedido'),
    url(r'^restar_menu_pedido/(?P<mesa>\d+)/(?P<menu>\d+)/(?P<pedido>\d+)$', restarMenuPedido, name='restar_menu_pedido'),





    # url(r'^menu_update/(?P<pk>\d+)$', MenuUpdate.as_view(), name='menu_update'),
    # url(r'^menu_delete/(?P<pk>\d+)$', MenuDelete.as_view(), name='menu_delete'),
    # # Categorias Productos
    # url(r'^categoria_create/$', categoriaCreate, name='categoria_create'),
    # # Actualizar Stock
    # url(r'^actualizar_stock/(?P<pk>\d+)$', actualizarStock, name='actualizar_stock'),

]
