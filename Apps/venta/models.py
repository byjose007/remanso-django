from django.db import models

# Create your models here.
from Apps.pedido.models import Pedido
from Apps.personal.models import Cliente


class Factura(models.Model):
    # cliente = models.ForeignKey(Cliente)
    pedido = models.ForeignKey(Pedido, on_delete=models.CASCADE)
    fecha = models.DateField(null=True,blank=True)
    tipo_pago = models.CharField(max_length=50,null=True, blank=True)
    iva = models.IntegerField()
    sub_total = models.FloatField()
    total = models.FloatField()

    def __str__(self):
        return '%s' % (self.pedido.cliente)
