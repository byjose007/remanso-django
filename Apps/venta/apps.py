from django.apps import AppConfig


class VentaConfig(AppConfig):
    name = 'Apps.venta'
